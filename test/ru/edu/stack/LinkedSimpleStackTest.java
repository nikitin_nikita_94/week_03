package ru.edu.stack;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedSimpleStackTest {

    public static final String FIRST_LINE = "FIRST";
    public static final String SECOND_LINE = "SECOND";
    public static final String THREE_LINE = "THREE";
    public static final String FORE_LINE = "FORE";

    public static SimpleStack<String> stack;

    @Before
    public void setUp() throws Exception {
        stack = new LinkedSimpleStack<>();
        stack.push(FIRST_LINE);
        stack.push(SECOND_LINE);
        stack.push(THREE_LINE);
    }

    @Test
    public void push() {
        assertNotNull(stack);

        assertEquals(3,stack.size());
        stack.push(FORE_LINE);
        assertEquals(4,stack.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void pushNullTest() {
        stack.push(null);
    }

    @Test
    public void pop() {
        assertNotNull(stack);

        assertEquals(3,stack.size());

        String current = stack.pop();
        assertEquals(THREE_LINE,current);
        assertEquals(2,stack.size());
    }

    @Test
    public void peek() {
        assertNotNull(stack);

        assertEquals(THREE_LINE,stack.peek());

        stack.pop();

        assertEquals(SECOND_LINE, stack.peek());
    }

    @Test
    public void empty() {
        assertEquals(false,stack.empty());

        stack.pop();
        stack.pop();
        stack.pop();

        assertEquals(true,stack.empty());
    }

    @Test
    public void size() {
        assertEquals(3,stack.size());

        stack.pop();

        assertEquals(2,stack.size());

        stack.push(FORE_LINE);

        assertEquals(3,stack.size());
    }

    @Test
    public void capacity() {
        assertEquals(-1,stack.capacity());
    }
}