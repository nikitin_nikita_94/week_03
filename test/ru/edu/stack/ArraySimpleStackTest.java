package ru.edu.stack;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleStackTest {

    public static final String FIRST_LINE = "FIRST";
    public static final String SECOND_LINE = "SECOND";
    public static final String THREE_LINE = "THREE";
    public static final String FOUR_LINE = "FOUR";
    public static final String FIVE_LINE = "FIVE";
    public static final String SIX_LINE = "SIX";

    public static SimpleStack<String> stack;

    @Before
    public void setUp() throws Exception {
        stack = new ArraySimpleStack<>(5);
        stack.push(FIRST_LINE);
        stack.push(SECOND_LINE);
        stack.push(THREE_LINE);
        stack.push(FOUR_LINE);
    }

    @Test
    public void push() {
        assertNotNull(stack);

        assertEquals(4, stack.size());
        stack.push(FIVE_LINE);
        assertEquals(5, stack.size());

        boolean isFalse = stack.push(SIX_LINE);
        assertEquals(false, isFalse);
    }

    @Test
    public void pop() {
        assertNotNull(stack);

        assertEquals(4, stack.size());

        String current = stack.pop();
        assertEquals(FOUR_LINE, current);
        assertEquals(3, stack.size());
    }

    @Test
    public void peek() {
        assertEquals(FOUR_LINE, stack.peek());

        stack.pop();

        assertEquals(THREE_LINE, stack.peek());
    }

    @Test
    public void empty() {
        assertEquals(false, stack.empty());

        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();

        assertEquals(true,stack.empty());
    }

    @Test
    public void size() {
        assertEquals(4, stack.size());

        stack.pop();

        assertEquals(3, stack.size());

        stack.push(SIX_LINE);

        assertEquals(4,stack.size());
    }

    @Test
    public void capacity() {
        assertEquals(1, stack.capacity());

        stack.pop();

        assertEquals(2, stack.capacity());
    }
}