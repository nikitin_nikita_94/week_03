package ru.edu.list;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ArraySimpleListTest {

    private static final String FIRST_WORD = "I";
    private static final String SECOND_WORD = "Love";
    private static final String THREE_WORD = "Java";
    private static final String UPDATE_WORD = "UPDATE";
    private static final int ZERO_INDEX = 0;
    private static final int FIRST_INDEX = 1;
    private static final int NEGATIVE_INDEX = -1;

    SimpleList<String> list = new ArraySimpleList<>();

    @Before
    public void setUp() throws Exception {
        list.add(FIRST_WORD);
        list.add(SECOND_WORD);
    }

    @Test
    public void add() {
        Assert.assertNotNull(list);
        Assert.assertEquals(2, list.size());

        list.add(THREE_WORD);
        Assert.assertEquals(3, list.size());
    }

    @Test
    public void set() {
        list.add(THREE_WORD);
        Assert.assertEquals(THREE_WORD, list.get(2));

        list.set(2, UPDATE_WORD);
        Assert.assertEquals(UPDATE_WORD, list.get(2));
    }

    @Test(expected = RuntimeException.class)
    public void setNoExistExceptionTest() {
        list.set(-1, FIRST_WORD);
        Assert.assertEquals(FIRST_WORD, list.get(0));
    }

    @Test
    public void get() {
        Assert.assertEquals(FIRST_WORD, list.get(ZERO_INDEX));
    }

    @Test(expected = RuntimeException.class)
    public void getNoExist() {
        Assert.assertEquals(FIRST_WORD, list.get(-1));
    }

    @Test(expected = RuntimeException.class)
    public void getNullTest() {
        Assert.assertEquals(FIRST_WORD, list.get(5));
    }

    @Test
    public void remove() {
        Assert.assertEquals(2, list.size());
        list.remove(ZERO_INDEX);
        Assert.assertEquals(1, list.size());
    }

    @Test(expected = RuntimeException.class)
    public void removeNoExistTest() {
        list.remove(-1);
    }

    @Test
    public void indexOf() {
        Assert.assertEquals(FIRST_INDEX, list.indexOf(SECOND_WORD));
        Assert.assertEquals(NEGATIVE_INDEX, list.indexOf(THREE_WORD));
    }

    @Test(expected = RuntimeException.class)
    public void indexOfNullTest() {
        list.indexOf(null);
    }

    @Test
    public void size() {
        Assert.assertEquals(2, list.size());
    }
}