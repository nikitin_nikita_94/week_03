package ru.edu.list;


import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;


public class LinkedSimpleListTest {

    public static final String FIRST_LINE = "FIRST";
    public static final String SECOND_LINE = "SECOND";
    public static final String THREE_LINE = "THREE";

    static SimpleList<String> list;

    @Before
    public void setUp() throws Exception {
        list = new LinkedSimpleList<>();
    }

    @Test
    public void add() {
        assertEquals(0, list.size());

        list.add(FIRST_LINE);
        assertEquals(1, list.size());
        assertEquals(FIRST_LINE, list.get(0));

        list.add(SECOND_LINE);
        assertEquals(2, list.size());
        assertEquals(SECOND_LINE, list.get(1));

    }

    @Test(expected = IllegalArgumentException.class)
    public void addNullTest() {
        list.add(null);
    }

    @Test
    public void set() {
        list.add(FIRST_LINE);
        assertEquals(FIRST_LINE, list.get(0));

        list.set(0, THREE_LINE);
        assertEquals(THREE_LINE, list.get(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setNegativeNumberTest() {
        list.add(FIRST_LINE);
        assertEquals(FIRST_LINE, list.get(0));

        list.set(-1, SECOND_LINE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setNullTest() {
        list.add(FIRST_LINE);
        assertEquals(FIRST_LINE, list.get(0));

        list.set(0, null);
    }

    @Test
    public void get() {
        list.add(FIRST_LINE);
        list.add(SECOND_LINE);

        assertEquals(FIRST_LINE, list.get(0));
        assertEquals(SECOND_LINE, list.get(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getNegativeNumberTest() {
        list.add(FIRST_LINE);
        list.add(SECOND_LINE);

        assertEquals(FIRST_LINE, list.get(0));
        assertEquals(SECOND_LINE, list.get(-1));
    }

    @Test
    public void remove() {
        list.add(FIRST_LINE);
        list.add(SECOND_LINE);
        list.add(THREE_LINE);

        assertEquals(3, list.size());
        list.remove(1);
        assertEquals(2, list.size());
        list.remove(0);
        assertEquals(1, list.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeNegativeNumberTest() {
        list.remove(-1);
    }

    @Test
    public void indexOf() {
        list.add(FIRST_LINE);
        list.add(SECOND_LINE);
        list.add(THREE_LINE);

        assertEquals(0, list.indexOf(FIRST_LINE));
        assertEquals(1, list.indexOf(SECOND_LINE));
        assertEquals(2, list.indexOf(THREE_LINE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void indexOfNullTest() {
        list.add(FIRST_LINE);
        list.add(SECOND_LINE);
        list.add(THREE_LINE);

        assertEquals(0, list.indexOf(null));
    }

    @Test
    public void size() {
        assertEquals(0, list.size());

        list.add(FIRST_LINE);
        list.add(SECOND_LINE);

        assertEquals(2, list.size());
    }
}