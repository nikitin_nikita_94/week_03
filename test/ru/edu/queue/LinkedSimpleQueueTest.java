package ru.edu.queue;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedSimpleQueueTest {

    public static final String FIRST_LINE = "FIRST";
    public static final String SECOND_LINE = "SECOND";
    public static final String THREE_LINE = "THREE";

    public static SimpleQueue<String> queue;

    @Before
    public void setUp() throws Exception {
        queue = new LinkedSimpleQueue<>();
        queue.offer(FIRST_LINE);
        queue.offer(SECOND_LINE);
    }

    @Test
    public void offer() {
        assertNotNull(queue);

        assertEquals(2,queue.size());

        queue.offer(THREE_LINE);
        assertEquals(3, queue.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void offerNullTest() {
        assertNotNull(queue);

        assertEquals(2,queue.size());

        queue.offer(null);
        assertEquals(3, queue.size());
    }

    @Test
    public void poll() {
        assertEquals(2,queue.size());

        String current = queue.poll();
        assertEquals(FIRST_LINE, current);
        assertEquals(1,queue.size());
    }

    @Test
    public void peek() {
        String current = queue.peek();

        assertEquals(FIRST_LINE,current);
    }

    @Test
    public void size() {
        assertEquals(2,queue.size());

        queue.offer(THREE_LINE);

        assertEquals(3,queue.size());
    }

    @Test
    public void capacity() {
        assertEquals(-1,queue.capacity());
    }
}