package ru.edu.queue;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleQueueTest {

    private static final Integer FIRST_EM = 10;
    private static final Integer SECOND_EM = 20;
    private static final Integer THREE_EM = 30;
    private static final Integer FOUR_EM = 30;

    ArraySimpleQueue<Integer> queue;

    @Before
    public void setUp() throws Exception {
        queue = new ArraySimpleQueue<>(5);
        queue.offer(FIRST_EM);
        queue.offer(SECOND_EM);
        queue.offer(THREE_EM);
        queue.offer(FOUR_EM);
    }

    @Test
    public void offer() {
        Assert.assertNotNull(queue);
        Assert.assertEquals(4, queue.size());

        queue.offer(50);
        Assert.assertEquals(5, queue.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void offerNullTest() {
        queue.offer(null);
    }

    @Test
    public void poll() {
        assertEquals(4, queue.size());
        assertEquals(FIRST_EM, queue.poll());
        assertEquals(3, queue.size());
    }

    @Test
    public void pollCapacityTwoTest() {
        SimpleQueue queue = new ArraySimpleQueue<Integer>(5);
        int lastPoll = -1;
        for (int i = 0; i < 10; ++i) {
            if (i > 4) {
                assertEquals(++lastPoll, queue.poll());
            }
            queue.offer(i);
        }
    }

    @Test
    public void peek() {
        assertEquals(FIRST_EM, queue.peek());
    }

    @Test
    public void size() {
        Assert.assertEquals(4, queue.size());
    }

    @Test
    public void capacity() {
        Assert.assertEquals(1, queue.capacity());
    }
}