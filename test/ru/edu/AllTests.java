package ru.edu;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ru.edu.list.ArraySimpleListTest;
import ru.edu.list.LinkedSimpleListTest;
import ru.edu.queue.ArraySimpleQueueTest;
import ru.edu.queue.LinkedSimpleQueueTest;
import ru.edu.stack.ArraySimpleStackTest;
import ru.edu.stack.LinkedSimpleStackTest;


@RunWith(Suite.class)
@Suite.SuiteClasses({
        ArraySimpleListTest.class,
        LinkedSimpleListTest.class,
        ArraySimpleQueueTest.class,
        LinkedSimpleQueueTest.class,
        ArraySimpleStackTest.class,
        LinkedSimpleStackTest.class
})
public class AllTests {
}
