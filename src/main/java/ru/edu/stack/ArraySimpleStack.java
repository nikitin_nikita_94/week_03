package ru.edu.stack;

public class ArraySimpleStack<T> implements SimpleStack<T> {

    /**
     * Длинна массива.
     */
    private int mSize;
    /**
     * Массив обьектов.
     */
    private T[] stackArray;
    /**
     * Вершина стека.
     */
    private int top;
    /**
     * Колличество элементов в стеке.
     */
    private int item;

    /**
     * Конструктор.
     * @param m
     */
    public ArraySimpleStack(final int m) {
        this.mSize = m;
        stackArray = (T[]) new Object[mSize];
        top = -1;
        item = 0;
    }
    /**
     * Добавление элемента в стэк.
     *
     * @param value элемент
     * @return true если удалось поместить элемент, false если места уже нет
     */
    @Override
    public boolean push(final T value) {
        if (value == null) {
            throw new IllegalArgumentException("Не коректное значение value");
        }
        if (item >= mSize) {
            return false;
        }
        stackArray[++top] = value;
        item++;
        return true;
    }

    /**
     * Получение и удаление элемента из стэка.
     *
     * @return элемент или null
     */
    @Override
    public T pop() {
        T elem = stackArray[top];
        stackArray[top] = null;
        item--;
        top--;
        return elem;
    }

    /**
     * Получение БЕЗ удаления элемента из стэка.
     *
     * @return элемент или null
     */
    @Override
    public T peek() {
        return stackArray[top];
    }

    /**
     * Проверка пустой ли стэк.
     *
     * @return true если пустой
     */
    @Override
    public boolean empty() {
        return top == -1;
    }

    /**
     * Количество элементов в стэке.
     *
     * @return количество
     */
    @Override
    public int size() {
        return item;
    }

    /**
     * Количество элементов которое может уместиться в стэке.
     *
     * @return емкость
     */
    @Override
    public int capacity() {
        return mSize - item;
    }
}
