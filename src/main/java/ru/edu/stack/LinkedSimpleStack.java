package ru.edu.stack;

public class LinkedSimpleStack<T> implements SimpleStack<T> {

    /**
     * Ссылка на первый элемент в списке.
     */
    private Link<T> first;

    /**
     * Количество элементов списке.
     */
    private int item;

    /**
     * Конструктор.
     */
    public LinkedSimpleStack() {
        first = null;
    }

    private class Link<T> {
        /**
         * Данные.
         */
        private T dData;

        /**
         * Ссылка на следующий элемент.
         */
        private Link<T> next;

        protected Link(final T value) {
            dData = value;
        }
    }
    /**
     * Добавление элемента в стэк.
     *
     * @param value элемент
     * @return true если удалось поместить элемент, false если места уже нет
     */
    @Override
    public boolean push(final T value) {
        if (value == null) {
            throw new IllegalArgumentException("Value равно NULL");
        }
        Link<T> current = new Link<>(value);
        current.next = first;
        first = current;
        item++;
        return true;
    }

    /**
     * Получение и удаление элемента из стэка.
     *
     * @return элемент или null
     */
    @Override
    public T pop() {
        Link<T> current = first;
        first = first.next;
        item--;
        return current.dData;
    }

    /**
     * Получение БЕЗ удаления элемента из стэка.
     *
     * @return элемент или null
     */
    @Override
    public T peek() {
        return first.dData;
    }

    /**
     * Проверка пустой ли стэк.
     *
     * @return true если пустой
     */
    @Override
    public boolean empty() {
        return first == null;
    }

    /**
     * Количество элементов в стэке.
     *
     * @return количество
     */
    @Override
    public int size() {
        return item;
    }

    /**
     * Количество элементов которое может уместиться в стэке.
     *
     * @return емкость
     */
    @Override
    public int capacity() {
        return -1;
    }
}
