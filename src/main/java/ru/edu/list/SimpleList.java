package ru.edu.list;

/**
 * Базовый интерфейс работы структуры данных "список".
 *
 * @param <T> тип хранимых элементов
 */
public interface SimpleList<T> {

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    void add(T value);

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    void set(int index, T value);

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    T get(int index);

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    void remove(int index);

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return int.
     */
    int indexOf(T value);

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    int size();
}
