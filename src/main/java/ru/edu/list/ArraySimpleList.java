package ru.edu.list;

import java.util.Arrays;

public class ArraySimpleList<T> implements SimpleList<T> {

    /**
     * Изначальная длина массива.
     */
    private static final int DEFAULT_CAPACITY = 10;

    /**
     * Массив обьектов.
     */
    private T[] arrayList;
    /**
     * Количество элементов в массиве.
     */
    private int nElements;

    /**
     * Конструктор.
     */
    public ArraySimpleList() {
        arrayList = (T[]) new Object[DEFAULT_CAPACITY];
    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final Object value) {
        if (arrayList.length == nElements) {
            arrayList = Arrays.copyOf(arrayList, (nElements + 1) * 2);
        }
        arrayList[nElements] = (T) value;
        nElements++;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final Object value) {
        if (index < 0) {
            throw new RuntimeException("Index = " + index + "negative number");
        }
        arrayList[index] = (T) value;
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        if (index < 0 || arrayList[index] == null) {
            throw new RuntimeException("Индекс меньше нуля или "
                    + "значение в массиве равно NULL");
        }
        return arrayList[index];
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        if (index < 0) {
            throw new RuntimeException("Index = " + index + "negative number");
        }

        if (index >= 0) {
            for (int i = index; i < arrayList.length - 1; i++) {
                arrayList[i] = arrayList[i + 1];
            }
            nElements--;
        }
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     */
    @Override
    public int indexOf(final Object value) {
        if (value == null) {
            throw new RuntimeException("Value is :" + value);
        }

        for (int i = 0; i < nElements; i++) {
            if (arrayList[i].equals(value)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return nElements;
    }
}
