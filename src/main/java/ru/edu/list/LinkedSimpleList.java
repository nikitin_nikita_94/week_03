package ru.edu.list;


public class LinkedSimpleList<T> implements SimpleList<T> {
    /**
     * Ссылка на первый элемент в списке.
     */
    private Link<T> first;

    /**
     * Ссылка на последний элемент в списке.
     */
    private Link<T> last;

    /**
     * Колличество элементов в списке.
     */
    private int size;

    /**
     * Конструктор.
     */
    public LinkedSimpleList() {
        first = null;
        last = null;
    }

    public static class Link<T> {
        /**
         * Данные списка.
         */
        private T dData;

        /**
         * Ссылка на следующий обьект списка.
         */
        private Link<T> next;

        /**
         * Ссылка на предыдущий обьект списка.
         */
        private Link<T> prev;

        /**
         * Конструктор.
         * @param value
         */
        public Link(final T value) {
            dData = value;
        }
    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {
        if (value == null) {
            throw new IllegalArgumentException("Значение value = NULL");
        }

        Link<T> newLink = new Link(value);
        if (first == null) {
            first = newLink;
            last = newLink;
        } else {
            newLink.prev = last;
            last.next = newLink;
            last = newLink;
        }
        size++;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        if (index < 0 || value == null) {
            throw new IllegalArgumentException("Индекс отрицательное "
                                                + "число или NULL");
        }
        findLink(index).dData = value;
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        if (index < 0) {
            throw new IllegalArgumentException("Индекс отрицательное число.");
        }

        Link<T> current = findLink(index);
        return current.dData;
    }

    /**
     * Вспомогательный метод по поиску обьекта.
     * @param index
     * @return возвращает обьек Link.
     */
    private Link<T> findLink(final int index) {
        Link<T> current = null;
        if (index < size / 2) {
            current = first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
        } else {
            current = last;
            for (int i = 0; i < (size - 1) - index; i++) {
                current = current.prev;
            }
        }
        return current;
    }


    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        if (index < 0) {
            throw new IllegalArgumentException("Индекс отрицательное число.");
        }

        removeElem(index);
        size--;
    }

    /**
     * Вспомогательный метод для удаления элемента.
     * @param index
     */
    private void removeElem(final int index) {
        if (size == 1) {
            first = null;
            last = null;
            return;
        }

        if (index == 0) {
            Link<T> newLink = first.next;
            if (newLink != null) {
                newLink.prev = null;
            }
            first = newLink;
            return;
        }

        if (index == size - 1) {
            Link<T> prev = last.prev;
            prev.next = null;
            last = prev;
        } else {
            Link<T> remove = findLink(index);
            remove.prev.next = remove.next;
            remove.next.prev = remove.prev;
        }
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     */
    @Override
    public int indexOf(final T value) {
        if (value == null) {
            throw new IllegalArgumentException("Значение Value равно Null.");
        }
        Link<T> current = first;
        Link<T> objValue = new Link<>(value);
        for (int i = 0; i < size; i++) {
            if (current.dData.equals(objValue.dData)) {
                return i;
            }
            current = current.next;
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }
}
