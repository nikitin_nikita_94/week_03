package ru.edu.queue;


public class LinkedSimpleQueue<T> implements SimpleQueue<T> {

    /**
     * Размер списка.
     */
    private int size;

    /**
     * Ссылка на первый обьект списка.
     */
    private Link<T> first;

    /**
     * Ссылка на последний обьект списка.
     */
    private Link<T> last;

    /**
     * Конструктор.
     */
    public LinkedSimpleQueue() {
        first = null;
        last = null;
    }

    public static class Link<T> {
        /**
         * Данные обьекта.
         */
        private T dData;

        /**
         * Ссылка на следующий обьект в списке.
         */
        private Link<T> next;

        /**
         * Конструктор.
         *
         * @param value
         */
        public Link(final T value) {
            dData = value;
        }
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (value == null) {
            throw new IllegalArgumentException("Значение value = NULL");
        }

        Link<T> current = new Link(value);
        if (first == null) {
            first = current;
        } else {
            last.next = current;
        }

        last = current;
        size++;
        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        Link<T> current = first;
        if (first.next == null) {
            last = null;
        }
        first = first.next;
        size--;

        return current.dData;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        return first.dData;
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return -1;
    }
}
