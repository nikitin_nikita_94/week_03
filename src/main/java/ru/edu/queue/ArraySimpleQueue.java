package ru.edu.queue;


public class ArraySimpleQueue<T> implements SimpleQueue<T> {

    /**
     * Хранит длинну массива.
     */
    private int maxCapacity;

    /**
     * Массив Элементов очереди.
     */
    private T[] arrayQue;

    /**
     * Переменная указывает на первый элемент массива.
     */
    private int front;

    /**
     * Количество обьектов в массиве.
     */
    private int arrayItems;

    /**
     * Конструктор.
     *
     * @param maxSize
     */
    public ArraySimpleQueue(final int maxSize) {
        this.maxCapacity = maxSize;
        arrayQue = (T[]) new Object[maxSize];
        front = 0;
        arrayItems = 0;
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (value == null) {
            throw new IllegalArgumentException("Очередь заполнена или"
                    + " значение value: "
                    + "некоректно.");
        }

        if (arrayItems == maxCapacity) {
            return false;
        }

        arrayQue[arrayItems++] = value;
        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        T temp = arrayQue[front];

        for (int i = 0; i < arrayQue.length - 1; i++) {
            arrayQue[i] = arrayQue[i + 1];
            if (i == arrayQue.length - 1) {
                arrayQue[arrayItems] = null;
            }
        }

        arrayItems--;
        return temp;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        T temp = arrayQue[front];
        return temp;
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return arrayItems;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return arrayQue.length - size();
    }
}
